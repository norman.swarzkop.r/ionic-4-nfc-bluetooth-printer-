(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./src/app/tab1/tab1.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/tab1/tab1.page.ts");







var Tab1PageModule = /** @class */ (function () {
    function Tab1PageModule() {
    }
    Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
            ],
            declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
        })
    ], Tab1PageModule);
    return Tab1PageModule;
}());



/***/ }),

/***/ "./src/app/tab1/tab1.page.html":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      DO Pengiriman Batubara <br/>\n      PT BSPC\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-list>\n    <ion-item>\n      <ion-label>No Tiket:</ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pTiket\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>PIT \t&#32;: </ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pPit\"></ion-input>\n    </ion-item> \n\n    <ion-item>\n      <ion-label>SEAM       :</ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pSeam\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>Kode Driver:</ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pKodedriver\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>Driver    :</ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pDriver\"></ion-input>\n    </ion-item> \n\n    <ion-item>\n      <ion-label>No Body    :</ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pNobody\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>No Pol     :</ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pNopol\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>Kontraktor :</ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pKontraktor\"></ion-input>\n    </ion-item> \n\n    <ion-item>\n      <ion-label>Checker :</ion-label>\n      <ion-input placeholder=\"Entry...\" [(ngModel)]=\"pChecker\"></ion-input>\n    </ion-item> \n\n    <ion-item>\n      <ion-label>Print QR Code</ion-label>\n      <ion-checkbox [(ngModel)]=\"checkQR\"></ion-checkbox>\n    </ion-item>\n    <br/>\n\n    <ion-button color=\"secondary\" expand=\"full\" (click)=\"createUser()\">Print</ion-button>\n\n  </ion-list>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/tab1/tab1.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiMS9GOlxcbm9ySU9OSUNcXHRlc3RcXG15QXBwL3NyY1xcYXBwXFx0YWIxXFx0YWIxLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2VsY29tZS1jYXJkIGlvbi1pbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/tab1/tab1.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var Tab1Page = /** @class */ (function () {
    function Tab1Page(events, navCtrl) {
        this.events = events;
        this.navCtrl = navCtrl;
        this.pTiket = '';
        this.pPit = '';
        this.pSeam = '';
        this.pKodedriver = '';
        this.pDriver = '';
        this.pNobody = '';
        this.pNopol = '';
        this.pKontraktor = '';
        this.pChecker = '';
        this.trig = false;
        this.getFormattedDate();
    }
    Tab1Page.prototype.createUser = function () {
        this.rawPrint();
        this.trig = true;
        this.events.publish('hello', this.rawData, this.trig);
        /*this.events.subscribe('hello', (this.sendData())=> {
          // user and time are the same arguments passed in `events.publish(user, time)`
          this.dataSend= panel11+" "+ time;
        });*/
    };
    Tab1Page.prototype.rawPrint = function () {
        this.getFormattedDate();
        this.rawData = '\x1B\x64\x01\x1B\x40\x1B\x74\x00\x1B\x61\x01\x1B\x21\x18DO Pengiriman Batubara\n';
        this.rawData += 'PT BSPC\n\n';
        this.rawData += '\x1B\x61\x00\x1B\x21\x08No tiket   : ' + this.pTiket + '\n';
        this.rawData += '\x1B\x61\x00\x1B\x21\x00PIT        : ' + this.pPit + '\n';
        this.rawData += 'SEAM       : ' + this.pSeam + '\n';
        this.rawData += '\x1B\x61\x00\x1B\x21\x08Kode Driver: ' + this.pKodedriver + '\n';
        this.rawData += 'Driver     : ' + this.pDriver + '\n';
        this.rawData += 'No Body    : ' + this.pNobody + '\n';
        this.rawData += '\x1B\x61\x00\x1B\x21\x00No Pol     : ' + this.pNopol + '\n';
        this.rawData += 'Kontraktor : ' + this.pKontraktor + '\n';
        this.rawData += 'Tgl        : ' + this.formattedDate + '\n';
        this.rawData += 'Jam        : ' + this.Time + '\n\n\n\n\n';
        this.rawData += 'Checker    : ' + this.pChecker + '\n\n';
        //this.rawData +='QR code :';
        if (this.checkQR == true) {
            var text = this.pTiket + ',' + this.pPit + ',' + this.pSeam + ',' + this.pKodedriver + ',' + this.pNobody + ',' + this.pKontraktor + ',' + this.fDate + ',' + this.Time;
            var dataq = {};
            var len = text.length + 3;
            dataq = ['\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07', '\x08', '\x09', '\x0A', '\x0B', '\x0C', '\x0D', '\x0E', '\x0F', '\x10', '\x11', '\x12', '\x13', '\x14',
                '\x15', '\x16', '\x17', '\x18', '\x19', '\x1A', '\x1B', '\x1C', '\x1D', '\x1E', '\x1F', '\x20', '\x21', '\x22', '\x23', '\x24', '\x25', '\x26', '\x27', '\x28', '\x29',
                '\x2A', '\x2B', '\x2C', '\x2D', '\x2E', '\x2F', '\x30', '\x31', '\x32', '\x33', '\x34', '\x35', '\x36', '\x37', '\x38', '\x39', '\x3A', '\x3B', '\x3C', '\x3D', '\x3E',
                '\x3F', '\x40', '\x41', '\x42', '\x43', '\x44', '\x45', '\x46', '\x47', '\x48', '\x49', '\x4A', '\x4B', '\x4C', '\x4D', '\x4E', '\x4F', '\x50', '\x51', '\x52', '\x53',
                '\x54', '\x55', '\x56', '\x57', '\x58', '\x59', '\x5A', '\x5B', '\x5C', '\x5D', '\x5E', '\x5F', '\x60', '\x61', '\x62', '\x63', '\x64', '\x65', '\x66', '\x67', '\x68',
                '\x69', '\x6A', '\x6B', '\x6C', '\x6D', '\x6E', '\x6F', '\x70', '\x71', '\x72', '\x73', '\x74', '\x75', '\x76', '\x77', '\x78', '\x79', '\x7A', '\x7B', '\x7C', '\x7D',
                '\x7E', '\x7F', '\x80', '\x81', '\x82', '\x83', '\x84', '\x85', '\x86', '\x87', '\x88', '\x89', '\x8A', '\x8B', '\x8C', '\x8D', '\x8E', '\x8F', '\x90', '\x91', '\x92'];
            this.rawData += '\x1B\x40\x1B\x74\x00\x1B\x40\x1B\x61\x01\x1D\x28\x6B\x04\x00\x31\x41\x32\x00\x1D\x28\x6B\x03\x00\x31\x43\x08\x1D\x28\x6B\x03\x00\x31\x45\x33\x1D\x28\x6B' + dataq[len] + '\x00\x31\x50\x30' + text + '\x1D\x28\x6B\x03\x00\x31\x51\x30\x0D\x0D';
            this.rawData += '\x1B\x61\x01\x1B\x21\x00' + this.pTiket + ' ';
            this.rawData += this.formattedDate + ' ';
            this.rawData += this.Time + '\n\n\n\n\n';
        }
        else {
            this.rawData += '\n\n\n';
        }
    };
    Tab1Page.prototype.getFormattedDate = function () {
        var dataObj = new Date();
        var year = dataObj.getFullYear().toString();
        var month = dataObj.getMonth().toString();
        var date = dataObj.getDate().toString();
        var hours = dataObj.getHours().toString();
        var minute = dataObj.getMinutes().toString();
        var second = dataObj.getSeconds().toString();
        var montArray = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        var minArray = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59'];
        var secArray = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59'];
        var mArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        this.formattedDate = date + ' ' + montArray[month] + ' ' + year;
        this.fDate = date + '-' + mArray[month] + '-' + year;
        this.Time = hours + ':' + minArray[minute] + ':' + secArray[second];
    };
    Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab1',
            template: __webpack_require__(/*! ./tab1.page.html */ "./src/app/tab1/tab1.page.html"),
            styles: [__webpack_require__(/*! ./tab1.page.scss */ "./src/app/tab1/tab1.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
    ], Tab1Page);
    return Tab1Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module.js.map