import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { NavController } from '@ionic/angular';
import { AlertController, ToastController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Platform, Events } from '@ionic/angular';
var Tab3Page = /** @class */ (function () {
    function Tab3Page(bluetoothSerial, navCtrl, alertCtrl, toastCtrl, scanner, platform, events) {
        var _this = this;
        this.bluetoothSerial = bluetoothSerial;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.scanner = scanner;
        this.platform = platform;
        this.events = events;
        //j: number;
        this.pairedDeviceID = 0;
        this.listToggle = false;
        this.listConn = false;
        this.listDisConn = false;
        this.dataSend = "";
        this.QRText = "";
        this.encodeText = '';
        this.encodeData = {};
        this.scannedData = {};
        //this.events.publish('hello', this.sendData());
        this.events.subscribe('hello', function (dataBuff, trig) {
            if (trig == true) {
                _this.sendRaw(dataBuff);
            }
        });
        this.checkBluetoothEnabled();
    }
    Tab3Page.prototype.exitApp = function () {
        //this.platform.
        //this.platform.navigator[‘app’].exitApp()
        //cordova.plugins.exit();
        navigator['app'].exitApp();
    };
    Tab3Page.prototype.checkBluetoothEnabled = function () {
        var _this = this;
        this.bluetoothSerial.isEnabled().then(function (success) {
            _this.listPairedDevices();
        }, function (error) {
            _this.showError("Please Enable Bluetooth");
        });
    };
    Tab3Page.prototype.listPairedDevices = function () {
        var _this = this;
        this.bluetoothSerial.list().then(function (success) {
            _this.pairedList = success;
            _this.listToggle = true;
            _this.listConn = true;
            _this.listDisConn = false;
            //this.encodeText='ok=  '+this.pairedList[this.index]+' '+ this.pairedDeviceID.length;
        }, function (error) {
            _this.showError("Please Enable Bluetooth");
            _this.listToggle = false;
            _this.listConn = false;
            _this.listDisConn = false;
        });
    };
    Tab3Page.prototype.selectDevice = function () {
        var connectedDevice = this.pairedList[this.pairedDeviceID];
        if (!connectedDevice.address) {
            this.showError('Select Paired Device to connect');
            return;
        }
        var address = connectedDevice.address;
        var name = connectedDevice.name;
        //this.events.publish('hello', this.connect(address));
        this.connect(address);
    };
    Tab3Page.prototype.connect = function (address) {
        var _this = this;
        this.bluetoothSerial.connect(address).subscribe(function (success) {
            _this.deviceConnected();
            _this.showToast("Successfully Connected");
            _this.listConn = false;
            _this.listDisConn = true;
        }, function (error) {
            _this.showError("Error:Connecting to Device");
            _this.listConn = true;
            _this.listDisConn = false;
        });
    };
    Tab3Page.prototype.deviceConnected = function () {
        var _this = this;
        this.bluetoothSerial.subscribe('\n').subscribe(function (success) {
            _this.handleData(success);
            _this.showToast("Connected Successfullly");
        }, function (error) {
            _this.showError(error);
        });
    };
    Tab3Page.prototype.deviceDisconnected = function () {
        this.bluetoothSerial.disconnect();
        this.showToast("Device Disconnected");
        this.listConn = true;
        this.listDisConn = false;
    };
    Tab3Page.prototype.handleData = function (data) {
        this.showToast(data);
    };
    Tab3Page.prototype.sendRaw = function (raw) {
        var _this = this;
        if (this.listDisConn == true) {
            //this.dataSend+='\n';
            //this.showToast(this.dataSend);
            this.bluetoothSerial.write(raw).then(function (success) {
                _this.showToast(success);
                //this.dataSend='';
            }, function (error) {
                _this.showError(error);
                //this.dataSend='';
            });
        }
        else {
            this.showToast("Device Disconnected");
        }
    };
    Tab3Page.prototype.sendData = function () {
        var _this = this;
        if (this.listDisConn == true) {
            this.dataSend += '\n';
            this.showToast(this.dataSend);
            this.bluetoothSerial.write(this.dataSend).then(function (success) {
                _this.showToast(success);
                _this.dataSend = '';
            }, function (error) {
                _this.showError(error);
                _this.dataSend = '';
            });
        }
        else {
            this.showToast("Device Disconnected");
        }
    };
    Tab3Page.prototype.printQR = function () {
        var _this = this;
        if (this.listDisConn == true) {
            var text = this.QRText;
            var dataq = {};
            var len = this.QRText.length + 3;
            dataq = ['\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07', '\x08', '\x09', '\x0A', '\x0B', '\x0C', '\x0D', '\x0E', '\x0F', '\x10', '\x11', '\x12', '\x13', '\x14',
                '\x15', '\x16', '\x17', '\x18', '\x19', '\x1A', '\x1B', '\x1C', '\x1D', '\x1E', '\x1F', '\x20', '\x21', '\x22', '\x23', '\x24', '\x25', '\x26', '\x27', '\x28', '\x29',
                '\x2A', '\x2B', '\x2C', '\x2D', '\x2E', '\x2F', '\x30', '\x31', '\x32', '\x33', '\x34', '\x35', '\x36', '\x37', '\x38', '\x39', '\x3A', '\x3B', '\x3C', '\x3D', '\x3E',
                '\x3F', '\x40', '\x41', '\x42', '\x43', '\x44', '\x45', '\x46', '\x47', '\x48', '\x49', '\x4A', '\x4B', '\x4C', '\x4D', '\x4E', '\x4F', '\x50', '\x51', '\x52', '\x53',
                '\x54', '\x55', '\x56', '\x57', '\x58', '\x59', '\x5A', '\x5B', '\x5C', '\x5D', '\x5E', '\x5F', '\x60', '\x61', '\x62', '\x63', '\x64', '\x65', '\x66', '\x67', '\x68',
                '\x69', '\x6A', '\x6B', '\x6C', '\x6D', '\x6E', '\x6F', '\x70', '\x71', '\x72', '\x73', '\x74', '\x75', '\x76', '\x77', '\x78', '\x79', '\x7A', '\x7B', '\x7C', '\x7D',
                '\x7E', '\x7F', '\x80', '\x81', '\x82', '\x83', '\x84', '\x85', '\x86', '\x87', '\x88', '\x89', '\x8A', '\x8B', '\x8C', '\x8D', '\x8E', '\x8F', '\x90', '\x91', '\x92'];
            this.bluetoothSerial.write('\x1B\x40\x1B\x74\x00\x1B\x40\x1B\x61\x01\x1D\x28\x6B\x04\x00\x31\x41\x32\x00\x1D\x28\x6B\x03\x00\x31\x43\x08\x1D\x28\x6B\x03\x00\x31\x45\x33\x1D\x28\x6B' + dataq[len] + '\x00\x31\x50\x30' + text + '\x1D\x28\x6B\x03\x00\x31\x51\x30\x0D\x0D')
                .then(function (success) {
                _this.showToast(success);
                _this.QRText = '';
            }, function (error) {
                _this.showError(error);
                _this.QRText = '';
            });
        }
        else {
            this.showToast("Device Disconnected");
        }
    };
    Tab3Page.prototype.QRprint = function () {
        var _this = this;
        if (this.listDisConn == true) {
            var text = this.QRText;
            var dataq = {};
            var len = this.QRText.length + 3;
            dataq = ['\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07', '\x08', '\x09', '\x0A', '\x0B', '\x0C', '\x0D', '\x0E', '\x0F', '\x10', '\x11', '\x12', '\x13', '\x14',
                '\x15', '\x16', '\x17', '\x18', '\x19', '\x1A', '\x1B', '\x1C', '\x1D', '\x1E', '\x1F', '\x20', '\x21', '\x22', '\x23', '\x24', '\x25', '\x26', '\x27', '\x28', '\x29',
                '\x2A', '\x2B', '\x2C', '\x2D', '\x2E', '\x2F', '\x30', '\x31', '\x32', '\x33', '\x34', '\x35', '\x36', '\x37', '\x38', '\x39', '\x3A', '\x3B', '\x3C', '\x3D', '\x3E',
                '\x3F', '\x40', '\x41', '\x42', '\x43', '\x44', '\x45', '\x46', '\x47', '\x48', '\x49', '\x4A', '\x4B', '\x4C', '\x4D', '\x4E', '\x4F', '\x50', '\x51', '\x52', '\x53',
                '\x54', '\x55', '\x56', '\x57', '\x58', '\x59', '\x5A', '\x5B', '\x5C', '\x5D', '\x5E', '\x5F', '\x60', '\x61', '\x62', '\x63', '\x64', '\x65', '\x66', '\x67', '\x68',
                '\x69', '\x6A', '\x6B', '\x6C', '\x6D', '\x6E', '\x6F', '\x70', '\x71', '\x72', '\x73', '\x74', '\x75', '\x76', '\x77', '\x78', '\x79', '\x7A', '\x7B', '\x7C', '\x7D',
                '\x7E', '\x7F', '\x80', '\x81', '\x82', '\x83', '\x84', '\x85', '\x86', '\x87', '\x88', '\x89', '\x8A', '\x8B', '\x8C', '\x8D', '\x8E', '\x8F', '\x90', '\x91', '\x92'];
            this.bluetoothSerial.write('\x1B\x40\x1B\x74\x00\x1B\x40\x1B\x61\x01\x1D\x28\x6B\x04\x00\x31\x41\x32\x00\x1D\x28\x6B\x03\x00\x31\x43\x08\x1D\x28\x6B\x03\x00\x31\x45\x33\x1D\x28\x6B' + dataq[len] + '\x00\x31\x50\x30' + text + '\x1D\x28\x6B\x03\x00\x31\x51\x30\x0D\x0D')
                .then(function (success) {
                _this.showToast(success);
                _this.QRText = '';
            }, function (error) {
                _this.showError(error);
                _this.QRText = '';
            });
        }
        else {
            this.showToast("Device Disconnected");
        }
    };
    Tab3Page.prototype.showError = function (error) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Error',
                            //subHeader: 'error',
                            message: error,
                            buttons: ['Dismiss']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Tab3Page.prototype.showToast = function (msj) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msj,
                            position: 'bottom',
                            duration: 1000
                        })];
                    case 1:
                        toast = _a.sent();
                        return [4 /*yield*/, toast.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Tab3Page.prototype.scan = function () {
        var _this = this;
        this.options = {
            prompt: 'Scan you barcode'
        };
        this.scanner.scan(this.options).then(function (data) {
            _this.scannedData = data;
        }, function (err) {
            _this.showError('Error');
        });
    };
    Tab3Page.prototype.encode = function () {
        var _this = this;
        this.scanner.encode(this.scanner.Encode.TEXT_TYPE, this.encodeText).then(function (data) {
            _this.encodeData = data;
            _this.dataSend = data;
            _this.bluetoothSerial.write(data);
            //this.dataSend=JSON.stringify(data);
        }, function (err) {
            _this.showToast(err);
        });
    };
    Tab3Page = tslib_1.__decorate([
        Component({
            selector: 'app-tab3',
            templateUrl: 'tab3.page.html',
            styleUrls: ['tab3.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [BluetoothSerial,
            NavController,
            AlertController,
            ToastController,
            BarcodeScanner,
            Platform,
            Events])
    ], Tab3Page);
    return Tab3Page;
}());
export { Tab3Page };
//# sourceMappingURL=tab3.page.js.map