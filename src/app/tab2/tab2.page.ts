import { Component } from '@angular/core';
import { NFC, Ndef } from '@ionic-native/nfc/ngx';
import { Subscription } from 'rxjs';
import { NavController, NavParams } from '@ionic/angular';
import { Events } from '@ionic/angular';
//import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  public navCtrl: NavController;
  public navParams: NavParams;
  public panel1:any;
  public panel2:any;
  public panel3:any;
  public panel4:any;
  public panel5:any;
  public panel6:any;
  public panel7:any;
  public panel8:any;

  public buff: any = [];
  public send: any = [];
  
  ndefMsg:      any=[];
  readingTag:   boolean   = false;
  writingTag:   boolean   = false;
  isWriting:    boolean   = false;
  subscriptions: Array<Subscription> = new Array<Subscription>(); 
  message:any = [];
 
  
  constructor 
  (
    private nfc: NFC, 
    private ndef: Ndef,
    public events: Events,

  )

  {

    this.subscriptions.push(this.nfc.addNdefListener()
    .subscribe(data => {
    
      if (this.readingTag) 
      {

        let record=data.tag.ndefMessage.length;
        for(let i=0; i<record; i++)
        {
          let payload=data.tag.ndefMessage[i].payload;
          this.buff[i] = this.nfc.bytesToString(payload).substring(3);
        }

        this.panel1=this.buff[0];
        this.panel2=this.buff[1];
        this.panel3=this.buff[2];
        this.panel4=this.buff[3];
        //this.panel4=JSON.stringify(data);
        this.readingTag = false;        
      }
      
      else if (this.writingTag) 
      {
     
        //this.nfc.write([this.ndefMsg[0]]);
        this.nfc.write(this.message)

        this.writingTag = false;
            
         /* .then(() => 
            {
              this.writingTag = false;
               
            })
            .catch(err => {
              this.panel1='error writing';
              this.writingTag = false;
            });*/
        //}
      }
    },
    err => 
    {
      this.panel1='No Tag...';
    })
    );  

  }
  

  proses() 
  { 
    
    this.message[0]=this.ndef.textRecord(this.panel5);
    this.message[1]=this.ndef.textRecord(this.panel6);
    this.message[2]=this.ndef.textRecord(this.panel7);
    this.message[3]=this.ndef.textRecord(this.panel8);

    this.writingTag = true;
  }

  tampil()
  {
    for(let i=0; i<4; i++)
    {
      this.buff[i] ="";
    }

    this.readTag();
  }


  ionViewWillLeave() 
  {
    this.subscriptions.forEach(sub => 
    {
      sub.unsubscribe();
    });
  }

  readTag() 
  {
    this.readingTag = true;
  }

  writeTag(writeText: string) 
  {
    this.writingTag = true;
  }

 


}


